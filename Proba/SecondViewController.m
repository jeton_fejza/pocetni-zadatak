//
//  SecondViewController.m
//  Proba
//
//  Created by Jeton Fejza on 6/3/14.
//  Copyright (c) 2014 Jeton Fejza. All rights reserved.
//

#import "SecondViewController.h"

@interface SecondViewController ()
@property (weak, nonatomic) IBOutlet UILabel *hiddenLabel;

- (IBAction)popFromNavigation:(UIButton *)sender;
- (IBAction)popToRoot:(id)sender;

@end

@implementation SecondViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if (self.shouldShowLabel) {
        self.hiddenLabel.hidden = NO;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)popFromNavigation:(UIButton *)sender {
    
    UINavigationController* nav=self.navigationController;
    [nav popViewControllerAnimated:YES];
}

- (IBAction)popToRoot:(id)sender {
    UINavigationController* nav=self.navigationController;
    [nav popToRootViewControllerAnimated:YES];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [super prepareForSegue:segue sender:sender];
    
    if ([segue.identifier isEqualToString:@"pushEkran2"]) {
        SecondViewController *vc = segue.destinationViewController;
        vc.shouldShowLabel = YES;
    }
}

@end
