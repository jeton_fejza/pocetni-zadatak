//
//  SecondViewController.h
//  Proba
//
//  Created by Jeton Fejza on 6/3/14.
//  Copyright (c) 2014 Jeton Fejza. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SecondViewController : UIViewController

@property (nonatomic, assign) BOOL shouldShowLabel;

@end
