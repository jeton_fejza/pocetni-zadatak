//
//  CompanyModel.h
//  Proba
//
//  Created by Jeton Fejza on 6/5/14.
//  Copyright (c) 2014 Jeton Fejza. All rights reserved.
//

#import "JSONModel.h"

@interface CompanyModel : JSONModel
@property NSString* name;
@end
