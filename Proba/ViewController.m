//
//  ViewController.m
//  Proba
//
//  Created by Jeton Fejza on 6/3/14.
//  Copyright (c) 2014 ___FULLUSERNAME___. All rights reserved.
//

#import "ViewController.h"
#import <SVProgressHUD.h>

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIButton *promijeniMeButton;

-(IBAction)change:(id)sender;
-(IBAction)animate:(id)sender;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)change:(id)sender
{
    UIButton *button = (UIButton *)sender;
    if ([button.titleLabel.text isEqualToString:@"Promijeni me"]){
        [button setTitle: @"Vrati me"  forState: UIControlStateNormal];
        [button setBackgroundColor:[UIColor redColor]];

    } else {
        [button setTitle: @"Promijeni me"  forState: UIControlStateNormal];
        [button setBackgroundColor:[UIColor whiteColor]];
    }

}

-(IBAction)animate:(id)sender{
    
    UIButton* button=self.promijeniMeButton;
    [UIView animateWithDuration:0.75 animations:^{
        button.transform = CGAffineTransformMakeTranslation(0, -200);
    } completion:^(BOOL finished) {
        [self performSelector:@selector(vratiPromijeniMeNaPocetni) withObject:nil afterDelay:2.0];
    }];
}

-(void) vratiPromijeniMeNaPocetni{
    self.promijeniMeButton.transform = CGAffineTransformIdentity;
}

@end
