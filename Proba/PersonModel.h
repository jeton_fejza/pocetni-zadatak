//
//  PersonModel.h
//  Proba
//
//  Created by Jeton Fejza on 6/5/14.
//  Copyright (c) 2014 Jeton Fejza. All rights reserved.
//

#import "JSONModel.h"
#import "CompanyModel.h"

@interface PersonModel : JSONModel
@property NSString* first_name;
@property NSString* last_name;
@property CompanyModel* company;
@property NSString* vehicle;

@end
