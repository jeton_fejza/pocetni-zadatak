//
//  AddNewViewController.m
//  Proba
//
//  Created by Jeton Fejza on 6/5/14.
//  Copyright (c) 2014 Jeton Fejza. All rights reserved.
//

#import "AddNewViewController.h"
#import "PersonModel.h"
#import "CompanyModel.h"
#import "TableViewController.h"

@interface AddNewViewController ()
@property (weak, nonatomic) IBOutlet UITextField *firstName;

@property (weak, nonatomic) IBOutlet UITextField *lastName;
@property (weak, nonatomic) IBOutlet UITextField *company;
@property (weak, nonatomic) IBOutlet UITextField *vehicle;
@property NSArray* people;
@end

@implementation AddNewViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)didEnterData:(id)sender {
    
    PersonModel* pm = [[PersonModel alloc] init];
    CompanyModel* cm = [[CompanyModel alloc] init];
    [cm setName:self.company.text];
    [pm setFirst_name:self.firstName.text];
    [pm setLast_name:self.lastName.text];
    [pm setCompany:cm];
    [pm setVehicle:self.vehicle.text];
    
    NSMutableArray* temp = [NSMutableArray arrayWithArray:self.people];
    [temp addObject:pm];
    self.people = [NSArray arrayWithArray:temp];
    
}


#pragma mark - Navigation


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [super prepareForSegue:segue sender:sender];
    if ([segue.identifier isEqualToString:@"ReturnSegue"]){
        [segue.destinationViewController setPeople:self.people];
        [segue.destinationViewController setDataAvailable:YES];
    }
}


@end
